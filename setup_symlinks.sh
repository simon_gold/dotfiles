#!/bin/bash

DIR=$(pwd)

# create symlinks for all dotfiles used in this repository
ln -s -f "${DIR}/.bashrc" ~/.bashrc
ln -s -f "${DIR}/.bash_aliases" ~/.bash_aliases
ln -s -f "${DIR}/.tmux.conf" ~/.tmux.conf
ln -s -f "${DIR}/.gitconfig" ~/.gitconfig
