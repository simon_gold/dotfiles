# MAGENTO
alias mage='bin/console'
alias magerun='bin/magerun2'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# tmux
alias t="tmux"
alias ta="t a -t"
alias tls="t ls"
alias tn="t new -t"
alias tk="tmux kill-session -t"

# git
alias gl="git log --oneline --decorate --all --graph"
alias gf="git fetch --all"
alias gc="git checkout"
alias gcrlf="git config --global core.autocrlf"
alias gcrlf:on="gcrlf true"
alias gcrlf:off="gcrlf false"

# ------------------------------------
# Docker alias and function
# ------------------------------------
alias do:up='drmf; do:do; docker-compose up -d'
alias do:do="docker volume ls -f dangling=true -q | xargs docker volume inspect -f '{{if .Labels}}{{else}}{{.Name}}{{end}}' | xargs docker volume rm"
alias drmf='docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)'

alias dps='docker ps --format "table {{.ID}}\t{{.Names}}\t{{.State}}\t{{.CreatedAt}}"'
